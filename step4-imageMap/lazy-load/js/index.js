document.addEventListener("DOMContentLoaded", () => {
    div = document.getElementById("div1");
    let lazyLoadImages = div.querySelectorAll("img.lazy");
    let lazyLoadThrottleTimeout;

    function lazyLoad() {
        if (lazyLoadThrottleTimeout) {
            clearTimeout(lazyLoadThrottleTimeout);
        }
        lazyLoadThrottleTimeout = setTimeout(() => {
            let scrollTop = window.pageYOffset;
            lazyLoadImages.forEach((i, j) => {
                if (i.offsetTop < (window.innerHeight + scrollTop)) {
                    i.src = i.dataset.src;
                    i.classList.remove('lazy');
                }
            });
            if (lazyLoadImages.length == 0) {
                document.removeEventListener("scroll", lazyLoad);
            }
        }, 200);
    }

    document.addEventListener("scroll", lazyLoad);
});

(function($) {
    $.fn.lazyload = function() {
        let that = $(this);
        let lazyLoadImages = that.find("img.lazy");
        let lazyLoadThrottleTimeout;

        if (lazyLoadThrottleTimeout) {
            clearTimeout(lazyLoadThrottleTimeout);
        }
        lazyLoadThrottleTimeout = setTimeout(() => {
            let scrollTop = that.parent().height();
            lazyLoadImages.each((i, j) => {
                    if ($(j).offset().top < scrollTop) {
                        j.src = j.dataset.src;
                        $(j).removeClass('lazy');
                    }
                })
                //console.log(that.parent().height());            
        }, 1000);
    }
})(jQuery);

$("#div2 div.container").on('scroll', function() {
    $(this).find('div.img').lazyload();
});

//use plugin of jquery
$("#div3 div.container").on('scroll', function() {
    $(this).find('div.img').find('img.lazy').lazy();
});

