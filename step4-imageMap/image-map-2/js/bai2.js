var toolTipText = document.getElementsByClassName("tooltip-text")[0];
document.querySelectorAll("map")[0].addEventListener("mouseover", function(e) {
    mouseOver();
})

document.querySelectorAll("map")[0].addEventListener("mouseout", function(e) {
    mouseOut();
})

document.querySelectorAll("map")[0].addEventListener("mousemove", function(e) {
    mouseMove(e);
})


function mouseMove(e) {
    toolTipText.style.top = e.clientY + 10 + "px";
    toolTipText.style.left = e.clientX + 10 + "px";
    toolTipText.innerText = e.target.getAttribute("alt");
}

function mouseOver() {
    toolTipText.style.visibility = "visible";
}

function mouseOut() {
    toolTipText.style.visibility = "hidden";
}