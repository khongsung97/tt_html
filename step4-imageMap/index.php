<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>index</title>
	</head>
	<body>
		<ul>
			<li><a href="image-map-1">imageMap 1</a></li>
			<li><a href="image-map-2">imageMap 2</a></li>
			<li><a href="drag-drop">drag drop</a></li>
			<li><a href="clip-path">clip-path</a></li>
			<li><a href="paralax">paralax</a></li>
			<li><a href="lazy-load">lazy load</a></li>
			<li><a href="lazy-load-jQuery">lazy load jquery</a></li>
		</ul>
	</body>
</html>