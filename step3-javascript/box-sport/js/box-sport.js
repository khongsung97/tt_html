var btnPrev = document.getElementsByClassName("btn-prev")[0];
var btnNext = document.getElementsByClassName("btn-next")[0];
var newsList = document.getElementById('news-list__ul');
var dots = document.getElementsByClassName("dotted");

//click btn prev and next
btnNext.addEventListener("click", function() {
    clickNext();
});

btnPrev.addEventListener("click", function() {
    clickPrevious();
});

//hover btn prev and next
btnPrev.addEventListener("mouseover", moveRight);
btnPrev.addEventListener("mouseout", function() {
    document.getElementById('news-list__ul').classList.remove("move-right-x");
});

btnNext.addEventListener("mouseover", moveLeft);
btnNext.addEventListener("mouseout", function() {
    document.getElementById('news-list__ul').classList.remove("move-left-x");
});

function moveRight() {
    document.getElementById('news-list__ul').classList.add("move-right-x");
}

function moveLeft() {
    document.getElementById('news-list__ul').classList.add("move-left-x");
}
//end hover


var i = 0;
var totalNewsSlide = newsList.children.length;

function clickNext() {
    i++;
    if (i == (totalNewsSlide / 3) - 1) {
        document.getElementById("news-list__ul").style.transform = 'translateX(-' + (200 * totalNewsSlide - 620 + 20) + 'px)';
        addBtnDisable("btn-next");
    } else {
        translateX(i);
        removeBtnDisable("btn-prev");
    }
}

function clickPrevious() {
    i--;
    if (i == 0) {
        translateX(0);
        addBtnDisable("btn-prev");
    } else {
        translateX(i);
        removeBtnDisable("btn-next");
    }
}

function removeBtnDisable(button) {
    document.getElementsByClassName(button)[0].classList.remove("btn-disable");
}

function addBtnDisable(button) {
    document.getElementsByClassName(button)[0].classList.add("btn-disable");
}

function translateX(argument) {
    document.getElementById("news-list__ul").style.transform = 'translateX(-' + (200 * 3 * argument) + 'px)';
}

window.onload = function() {
    newsList.firstElementChild.style.marginLeft="0";
}