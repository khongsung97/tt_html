window.onload = function() {
    var btnPrev = document.getElementsByClassName("btn-prev")[0];
    var btnNext = document.getElementsByClassName("btn-next")[0];
    var newsList = document.getElementById('news-list__ul');
    var dots = document.getElementsByClassName("dotted");

    //click btn prev and next
    btnNext.addEventListener("click", function() {
        clickNext();
    });

    btnPrev.addEventListener("click", function() {
        clickPrevious();
    });

    //hover btn prev and next
    btnPrev.addEventListener("mouseover", moveRight);
    btnPrev.addEventListener("mouseout", function() {
    	document.getElementById('news-list__ul').classList.remove("move-right-x");
    });

    btnNext.addEventListener("mouseover", moveLeft);
    btnNext.addEventListener("mouseout", function() {
    	document.getElementById('news-list__ul').classList.remove("move-left-x");
    });

    function moveRight() {
    	document.getElementById('news-list__ul').classList.add("move-right-x");
    }

    function moveLeft() {
    	document.getElementById('news-list__ul').classList.add("move-left-x");
    }
    //end hover

    //click dots
    for (let j = 0; j < dots.length; j++) {
        dots.item(j).addEventListener("click", function() {            
            let num = this.getAttribute("value");
            dotsHighLight(num);
            activeDotted(num);
            i = j;
        });
    }
}

var i =0;


function clickNext() {
	i++;
    if (i == 4) {
        document.getElementById("news-list__ul").style.transform = 'translateX(-' + (284 * 10 - 700 + 20) + 'px)';
        addBtnDisable("btn-next");
    } else {
        translateX(i);
        removeBtnDisable("btn-prev");
    }
    activeDotted(i);
}

function clickPrevious() {
	i--;
    if (i == 0) {
        translateX(0);
        addBtnDisable("btn-prev");
    } else {
        translateX(i);
        removeBtnDisable("btn-next");
    }
    activeDotted(i);
}

// dots control
function activeDotted(j) {
	var dots = document.getElementsByClassName("dotted");
	for (var i = 0; i < dots.length; i++) {
		dots[i].classList.remove("slide-active");
	}
	dots[j].classList.add("slide-active");
}


function dotsHighLight(j) {
	switch (j) {
        case "4":
            document.getElementById("news-list__ul").style.transform = 'translateX(-' + (284 * 2 * 5 - 700 + 20) + 'px)';
            addBtnDisable("btn-next");
            removeBtnDisable("btn-prev");
            break;
        case "0":
            translateX(0)
            addBtnDisable("btn-prev");
            removeBtnDisable("btn-next");
            break;
        default:
            translateX(j)
            removeBtnDisable("btn-next");
            removeBtnDisable("btn-prev");
            i = j
	}
}

function removeBtnDisable (button) {
	document.getElementsByClassName(button)[0].classList.remove("btn-disable");
}

function addBtnDisable(button) {
	document.getElementsByClassName(button)[0].classList.add("btn-disable");
}

function translateX(argument) {
	document.getElementById("news-list__ul").style.transform = 'translateX(-' + (284 * 2 * argument) + 'px)';
}