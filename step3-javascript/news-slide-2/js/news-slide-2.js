(function($) {
    $.fn.slide = function(option) {
        let setting = $.extend({
            step: 2
        }, option);

        let that = this;
        let control = that.find('.control');
        let gallery = that.find('.gallery');
        let newsList = that.find('.news-list');
        let item = that.find('.article');
        let curren = 0;
        let margin = 20;
        let step = setting.step;
        let dots = '<span class="dotted"></span>';
        let dotsNum = Math.ceil(item.length / step);

        gallery.css('width', item.parent().width() * item.length + 20);

        if (dotsNum > 0) {
            control.html(dots.repeat(dotsNum));
            that.find('.dotted').first().addClass('slide-active');
        }

        that.find('.btn-next').click(function() {
            let xLastMove = (item.width() + margin) * item.length - newsList.width() + margin;
            curren++;
            activeDot(curren);
            if (curren >= (dotsNum - 1)) {
                gallery.css('transform', 'translateX(-' + xLastMove + 'px)');
                $(this).addClass('btn-disable');
                curren = dotsNum - 1;
            } else {
                moveSlide(curren);
            }
        });

        that.find('.btn-prev').click(function() {
            curren--;
            activeDot(curren);
            if (curren <= 0) {
                moveSlide(curren);
                $(this).addClass('btn-disable');
                curren = 0;
            } else {
                moveSlide(curren);
            }
        });

        that.find('.dotted').click(function() {
            let index = ($(this).index());
            activeDot(index);
            moveSlide(index);
            curren = index;
            if (index >= dotsNum) {
                that.find('.btn-next').addClass('btn-disable');
            } else if( index <= 0) {
                that.find('.btn-prev').addClass('btn-disable');
            }
        });

        function moveSlide(i) {
            let xMove = (item.width() + margin) * step * i;
            gallery.css('transform', 'translateX(-' + xMove + 'px)');
            if (i > 0) {
                that.find('.btn-next').removeClass('btn-disable');
                that.find('.btn-prev').removeClass('btn-disable');
            }
        }

        function activeDot(curren) {
            that.find('.dotted.slide-active').removeClass('slide-active');
            that.find('.dotted').eq(curren).addClass('slide-active');
        }

        return this;
    }

})(jQuery);

$(document).ready(function() {
    $('#news-lide1').slide({
        step: 3
    });
    $('#news-lide2').slide({
        step: 2
    });
});