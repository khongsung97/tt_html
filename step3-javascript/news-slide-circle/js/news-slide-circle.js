window.onload = function() {
    var btnPrev = document.getElementsByClassName("btn-prev")[0];
    var btnNext = document.getElementsByClassName("btn-next")[0];
    var newsList = document.getElementById('news-list__ul');
    var dots = document.getElementsByClassName("dotted");

    //click btn prev and next
    btnNext.addEventListener("click", function() {
        next();
    });

    btnPrev.addEventListener("click", function() {
        previous();
    });

    //hover btn prev and next
    btnPrev.addEventListener("mouseover", moveRight);
    btnPrev.addEventListener("mouseout", function() {
    	document.getElementById('news-list__ul').classList.remove("move-right-x");
    });

    btnNext.addEventListener("mouseover", moveLeft);
    btnNext.addEventListener("mouseout", function() {
    	document.getElementById('news-list__ul').classList.remove("move-left-x");
    });

    function moveRight() {
    	document.getElementById('news-list__ul').classList.add("move-right-x");
    }

    function moveLeft() {
    	document.getElementById('news-list__ul').classList.add("move-left-x");
    }
    //end hover

    //click dots
    for (var i = 0; i < dots.length; i++) {
        dots.item(i).addEventListener("click", function() {            
            let num = this.getAttribute("value");
            dotsHighLight(num);
            activeDotted(num);
        });
    }
}

var i =0;

function next() {
    i++;
    if (i > 3) {
        document.getElementById('news-list__ul').className = '';
        i = 0;
        document.getElementById('news-list__ul').style.transform = 'translateX(-' + (284 * 2 * i + 284) + 'px)';
        window.setTimeout(function() {
            document.getElementById('news-list__ul').className = 'gallery';
            i++;
            document.getElementById('news-list__ul').style.transform = 'translateX(-' + (284 * 2 * i + 284) + 'px)';
        }, 10);
        activeDotted(i+1);
    } else {
        document.getElementById('news-list__ul').style.transform = 'translateX(-' + (284 * 2 * i + 284) + 'px)';
        i==3?activeDotted(0):activeDotted(i);
    }
}

function previous() {
    i--;
    if (i < 0) {
        document.getElementById('news-list__ul').className = '';
        i = 3;
        document.getElementById('news-list__ul').style.transform = 'translateX(-' + (284*2 * i+284) + 'px)';
        window.setTimeout(function() {
            document.getElementById('news-list__ul').className = 'gallery';
            i--;
            document.getElementById('news-list__ul').style.transform = 'translateX(-' + (284*2 * i+284) + 'px)';
        }, 10);
        activeDotted(2);

    } else {
        document.getElementById('news-list__ul').style.transform = 'translateX(-' + (284*2 * i+284) + 'px)';
        activeDotted(i);
    }
}

// dots control
function activeDotted(j) {
	var dots = document.getElementsByClassName("dotted");
	for (var i = 0; i < dots.length; i++) {
		dots[i].classList.remove("slide-active");
	}
	dots[j].classList.add("slide-active");
}


function dotsHighLight(j) {
	switch (j) {
        case "4":
            document.getElementById("news-list__ul").style.transform = 'translateX(-' + (284 * 2 * 5 - 700 + 20) + 'px)';
            addBtnDisable("btn-next");
            removeBtnDisable("btn-prev");
            break;
        case "0":
            translateX(0)
            addBtnDisable("btn-prev");
            removeBtnDisable("btn-next");
            break;
        default:
            translateX(j)
            removeBtnDisable("btn-next");
            removeBtnDisable("btn-prev");
            i = j
	}
}

function removeBtnDisable (button) {
	document.getElementsByClassName(button)[0].classList.remove("btn-disable");
}

function addBtnDisable(button) {
	document.getElementsByClassName(button)[0].classList.add("btn-disable");
}

function translateX(argument) {
	document.getElementById("news-list__ul").style.transform = 'translateX(-' + (284 * 2 * argument + 284) + 'px)';
}