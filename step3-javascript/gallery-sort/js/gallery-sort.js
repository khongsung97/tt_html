
window.onload = function() {
	// modal
	var modal = document.getElementById("modal-gallery");

	//img in modal
	var modalImg = document.getElementsByClassName("gallery-sort__modal--content")[0];
	
	//ul cover img
	var lists = document.getElementsByClassName("gallery-sort__lists")[0];
	
	//btn close modal
	var close = document.getElementsByClassName("gallery-sort__modal--close")[0];
	
	// caption modal
	var caption = document.getElementsByClassName("gallery-sort__modal--caption")[0];

	// foreach item in ul > li.click > get src
	for (let i = 0; i < lists.children.length; i++) {
		lists.children.item(i).addEventListener("click", function() {
			modal.style.display = "block";
			modalImg.src = this.children[0].getAttribute("src");
			caption.innerHTML = document.getElementsByClassName("gallery-sort__desc")[0].children[0].innerText;
		})
	}

	// close modal
	close.addEventListener("click", function() {
		modal.style.display = "none";
	})
	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}
}