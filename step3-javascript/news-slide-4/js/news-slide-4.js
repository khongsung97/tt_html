var i = 0;
var btnUp = document.getElementsByClassName("insbox__control--up")[0];
var btnDown = document.getElementsByClassName("insbox__control--down")[0];
var txt = document.getElementsByClassName("insbox__control--text")[0];
var totalNews = document.getElementById("insbox__ul").children.length;

btnUp.addEventListener('click', function() {
    moveUp(i);
    console.log(i);
    txt.innerHTML = i + 1 + '/' + totalNews;
});
btnDown.addEventListener('click', function() {
    moveDown(i);
    console.log(i);
    txt.innerHTML = i + 1 + '/' + totalNews;
});

function moveDown() {
    i++;
    if (i > totalNews-1) {
        document.getElementById("insbox__ul").style.transform = 'translateY(-' + 156 * (totalNews-1) + 'px)';
        i = totalNews-1;
    } else {
        document.getElementById("insbox__ul").style.transform = 'translateY(-' + 156 * i + 'px)';
    }

}

function moveUp() {
    i--;
    document.getElementById("insbox__ul").style.transform = 'translateY(-' + 156 * i + 'px)';
    if (i < 0) {
        i = 0;
    }
}

window.onload =function() {
	txt.innerHTML = i + 1 + '/' + totalNews;
	if (totalNews==1) {
		document.getElementsByClassName("insbox__control")[0].style.display = "none";
	}
}